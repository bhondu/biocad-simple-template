1. Download and install Node.js:
    [https://nodejs.org](https://nodejs.org)
1. Update npm to the latest version:
    `npm install --global npm@latest`
1. Clone repository:
    `git clone https://bitbucket.org/bhondu/biocad-simple-template.git`
1. Checkout master branch:
    `git checkout master`
1. Install npm packages:
    `npm install`
1. Run tests:
    `npm test`
