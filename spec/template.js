var path = require('path');
var expect = require('chai').expect;

var Template = require(path.join(__dirname, '..', './template.js'));

describe('Template', function () {
  'use strict';
  var tpl;

  var countDaysSince = function (dateString) {
    // return number of days passed since the given date (rounded down to integer value)
    return Math.floor((new Date() - new Date(dateString)) / 1000 / 60 / 60 / 24);
  };

  // fixme: 12167 is for 2015-11-06; update value accordingly when running tests
  var testDateString = '1982-07-15';
  var testDaysCountInt = 12167;

  var testDataObj = {
    value1: 'VALUE1',
    value2: 'VALUE2',
    func: function () {
      return 'FUNC';
    },
    funcParam: function (arg) {
      return 'funcParam=' + arg;
    }
  };

  beforeEach(function () {
    tpl = Template.create('firstText_${value1}_secondText_${value2}_thirdText_${func()}_${funcParam(value1)}_lastText');
  });


  it('Template exists', function () {
    expect(Template).to.be.an('object');
  });

  it('Template has create method', function () {
    expect(Template.create).to.be.a('function');
  });

  it('Template.create returns a function', function () {
    expect(tpl).to.be.a('function');
  });

  it('template could run', function () {
    expect(tpl(testDataObj)).to.be.a('string');
  });

  it('tests a really simple template', function () {
    var tpl = Template.create("text");
    expect(tpl({})).to.equal('text');
  });

  it('tests a template with different params', function () {
    expect(tpl(testDataObj)).to.equal('firstText_VALUE1_secondText_VALUE2_thirdText_FUNC_funcParam=VALUE1_lastText');
  });

  it('tests countDaysSince', function () {
    expect(countDaysSince(testDateString)).to.equal(testDaysCountInt);
  });

  it('tests a template with countDaysSince', function () {
    var tpl = Template.create("I am ${countDaysSince('" + testDateString + "')} days old.");
    expect(tpl({
      countDaysSince: countDaysSince
    })).to.equal('I am ' + testDaysCountInt + ' days old.');
  });

  it('tests a template with no text', function () {
    var tpl = Template.create("${countDaysSince('" + testDateString + "')}");
    expect(tpl({countDaysSince: countDaysSince})).to.equal(testDaysCountInt.toString());
  });

  it('tests a template with $ symbol', function () {
    var tpl = Template.create("$");
    expect(tpl({})).to.equal('$');
  });

  it('tests a template with empty expression ${}', function () {
    var tpl = Template.create("${}");
    expect(tpl({})).to.equal('');
  });

  // todo: add more tests for more complex usage

  // Add more assertions here
});
