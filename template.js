'use strict';

var Template;
Template = (function () {

  /** -- private properties */

  // RegExp to extract expressions from template text
  var re = re = /\$\{([^}]*)\}/g,
    boundary = '#';

  var fnParamName = 'obj';


  /** -- public API */

  return {
    /**
     * Creates a simple template from string.
     * @param {string} text Contains template text
     * @returns {function(data:Object, template:string)}
     * */
    create: function (text) {
      // parts come in pairs: ['someText', someExpression]
      var parts = text
        .replace(re, boundary + '$1' + boundary)
        .split(boundary);

      var fnBody = 'var result; with(' + fnParamName + ') { result = ';

      for (var i = 0; i < parts.length; i++) {
        fnBody += createPart(parts[i], i % 2);
      }

      fnBody += ';}; return result;';

      //console.log(parts);
      //console.log(fnBody);

      // create template fn: function(fnParamName) { fnBody };
      return new Function(fnParamName, fnBody);
    }
  };

  /** -- private methods (hoisted up) */

  function escapeQuotes(text) {
    // todo: improve this simple logic for tricky strings with many nested quotes
    return text
      .replace(/'/, "\\'")
      .replace(/"/, '\\"');
  }

  function createPart(text, isExpression) {
    // todo: think about more strict escaping to improve safety
    if (isExpression) {
      // wrap text as JS expression
      return '+(' + (text || "''") + ')+';
    } else {
      // wrap text in single quotes for later concatenation
      return "'" + escapeQuotes(text) + "'";
    }
  }

})();

if (typeof module !== "undefined") {
  module.exports = Template;
}
